###############################################################################
#
# Copyright 2023 NVIDIA Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
###############################################################################

# This role was forked from the dpu-devops-kit and customized for nvcert
# Ubuntu 20.04 host install
- name: Setup working directory on host
  set_fact:
    local_dir: "/tmp/dpu_nvcert_bootstrap"

- name: Create a working dir
  file:
    path: "{{ local_dir }}"
    state: directory
    mode: 0755
    recurse: yes

- name: Ubuntu 20.04 install
  set_fact:
    doca_file: '{{ doca_host_ubuntu2004_file }}'
  when:
    - ansible_distribution == 'Ubuntu' and ansible_distribution_version == '20.04'

- name: Get inventory file name
  command: /usr/bin/env python3
  args:
    stdin: |
        import json
        my_inventory_name = "{{ hostvars['x86']['inventory_file'] }}"
        print(my_inventory_name.split("/")[-1])
  register: my_inventory_name_str
  delegate_to: localhost

- name: Set jumphost path suffix
  set_fact:
    local_path_suffix: "{{ my_inventory_name_str.stdout.strip() }}"
  delegate_to: localhost

- name: Setup working directory on test-server/jumphost
  set_fact:
    jump_host_dir: "/tmp/dpu_nvcert_bootstrap/{{ local_path_suffix }}"
  delegate_to: localhost

# Ubuntu 22.04 host install
- name: Get ubuntu 22.04 doca-host file name
  set_fact:
    doca_file: '{{ doca_host_ubuntu2204_file }}'
  when:
    - ansible_distribution == 'Ubuntu' and ansible_distribution_version == '22.04'

- name: Checking if DOCA file for Ubuntu host has been downloaded
  stat:
    path: '{{ local_dir }}/{{ doca_file }}'
  register: doca_host_file

- name: Copy DOCA-host package from jumphost if it is present there
  block:
    - name: Checking if the DOCA-host file is present on the jump host
      stat:
        path: '{{ jump_host_dir }}/{{ doca_file }}'
      register: doca_jump_host_file
      delegate_to: localhost

    - name: Copy DOCA-host file from jump host to x86
      copy:
        src: '{{ jump_host_dir }}/{{ doca_file }}'
        dest: '{{ local_dir }}/'
      when: doca_jump_host_file.stat.exists
      delegate_to: x86
  when: not doca_host_file.stat.exists

- name: Re-run DOCA host file check
  stat:
    path: '{{ local_dir }}/{{ doca_file }}'
  register: doca_host_file

- name: Download DOCA host file from URL
  get_url:
    url: '{{ doca_host_url }}/{{ doca_file }}'
    dest: '{{ local_dir }}'
  when: not doca_host_file.stat.exists

- name: Install DOCA Ubuntu host
  apt:
    deb: '{{local_dir }}/{{ doca_file }}'
    dpkg_options: 'force-overwrite'

- name: Install Ubuntu Host DOCA components...this takes a while
  apt:
    update_cache: yes
    name:
      - doca-sdk
      - doca-runtime
      - doca-tools
      - doca-extra
