# Match DPU Serial Numbers
Check if the DPU SN from the BMC is present on the server. If it is not the DPU
may not be hosted by the server.

