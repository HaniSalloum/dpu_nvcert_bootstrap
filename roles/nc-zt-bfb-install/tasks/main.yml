###############################################################################
#
# Copyright 2023 NVIDIA Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
###############################################################################

# This role was forked from the dpu-devops-kit and customized for DPU Nvcert.
# These tasks are intended to be run on the DPU BMC. They install the DPU
# BFB.

- name: Get inventory file name
  command: /usr/bin/env python3
  args:
    stdin: |
        import json
        my_inventory_name = "{{ hostvars['x86']['inventory_file'] }}"
        print(my_inventory_name.split("/")[-1])
  register: my_inventory_name_str
  delegate_to: localhost

- name: Set local path suffix
  set_fact:
    local_path_suffix: "{{ my_inventory_name_str.stdout.strip() }}"
  delegate_to: localhost

- name: Print local path suffix
  debug:
    msg: "{{ local_path_suffix }}"
  delegate_to: localhost

- name: Setup working directory on local host
  set_fact:
    local_dir: "/tmp/dpu_nvcert_bootstrap/{{ local_path_suffix }}"
  delegate_to: localhost

- name: Create a working dir
  file:
    path: "{{ local_dir }}"
    state: directory
    mode: 0755
    recurse: yes
  delegate_to: localhost

- name: Check if the DPU BMC access is authorized by issuing a redfish call
  import_role:
    name: nc-check-dpu-bmc-access
  delegate_to: localhost

- name: Change the first time BMC password (0penBmc)
  import_role:
    name: nc-config-dpu-bmc-password
  when: (bmc_access_unauthorized is defined) and (bmc_access_unauthorized|bool == true)
  delegate_to: localhost

- name: Update BMC version
  import_role:
    name: nc-update-bmc
  when: bmc_fw_ver != bmc_version
  delegate_to: localhost

- name: Add ssh-rsa algorithm to work with older SSL versions
  set_fact:
    ssh_extra_args: "-oHostKeyAlgorithms=+ssh-rsa"
  delegate_to: localhost

- name: Attempt ssh to DPU BMC and update ssh key if needed
  command: /usr/bin/env python3
  args:
    stdin: |
       import os
       import re
       import subprocess
       cmd = "sshpass -p {{ ansible_password }} ssh -oStrictHostKeyChecking=no {{ssh_extra_args}} {{ ansible_user }}@{{ ansible_host }}"
       ssh_output = subprocess.run(cmd.split(), capture_output=True)
       ssh_output = ssh_output.stderr.decode()
       for line in ssh_output.splitlines():
           line = line.strip()
           if re.match("ssh-keygen", line):
               os.system(line)
  delegate_to: localhost

- name: Get DPU Serial Number from the BMC
  include_role:
    name: nc-get-dpu-sn-from-bmc

- name: Check if the DPU is hosted by the server
  include_role:
    name: nc-match-dpu-bmc-to-host

- name: Abort play if a Serial Number mismatch is detected
  debug:
    msg: "Abort play; DPU BMC {{ hostvars['dpu_bmc']['dpu_sn_from_bmc'] }} is not hosted by this server"
  when: dpu_sn_from_host == 'unknown'
  failed_when: dpu_sn_from_host == 'unknown'

- name: Stop rshim on the BMC 1/2
  ansible.builtin.raw: /bin/bash -c "systemctl stop rshim"

- name: Start rshim on the BMC 2/2
  ansible.builtin.raw: /bin/bash -c "systemctl start rshim"

- name: Pause 30 seconds for rshim
  pause:
    seconds: 30

- name: Get the device info
  command: 'sshpass -p "{{ ansible_password }}" ssh -oStrictHostKeyChecking=no {{ssh_extra_args}} {{ ansible_user }}@{{ ansible_host }} grep DEV_INFO /dev/rshim0/misc'
  register: dev_info
  delegate_to: localhost

- name: Get the device type
  command: /usr/bin/env python3
  args:
    stdin: |
       dev_info = "{{ dev_info.stdout.strip() }}"
       if "BlueField-2" in dev_info:
            dev_type_out = "BF2"
       else:
            dev_type_out = "BF3"
       print(dev_type_out)
  register: dev_type_out
  delegate_to: localhost

- name: Check if BF2 or BF3
  set_fact:
    bf_type: "{{ dev_type_out.stdout.strip() }}"

- name: Parse device type
  set_fact:
    dev_type: "{{ dev_type_out.stdout.strip() }}-{{ board_type }}"

- name: Locate the BFB image URL
  import_role:
    name: nc-locate-bfb
  delegate_to: localhost

- name: Checking if BFB has been downloaded
  stat:
    path: "{{ local_dir }}/{{ doca_bfb }}"
  register: bfbdownload
  delegate_to: localhost

- name: Download bfb from the Internet
  get_url:
    url: "{{ bfb_download_url }}"
    dest: "{{ local_dir }}"
  delegate_to: localhost
  when: not bfbdownload.stat.exists

- name: Combine bf.cfg and downloaded BFB into new.bfb
  block:
    - name: change the DPU rshim mac
      set_fact:
        change_dpu_rshim_mac: false

    - name: Role - Build bfb.cfg
      import_role:
        name: nc-build-bfb-cfg
      delegate_to: localhost

    - name: Add bf.cfg to BFB file
      shell: cat {{ local_dir }}/{{ doca_bfb }} {{ local_dir }}/bf.cfg  > {{ local_dir }}/new.bfb
      delegate_to: localhost

- name: Set DOCA bfb file
  ansible.builtin.set_fact:
    prod_doca_bfb: new.bfb

# This process takes over 20 mins
- name: SCP BFB to Server under tests ...this will take a while
  command: 'sshpass -p "{{ ansible_password }}" scp -oStrictHostKeyChecking=no {{ssh_extra_args}} {{ local_dir }}/{{ prod_doca_bfb }} {{ ansible_user }}@{{ ansible_host }}:/dev/rshim0/boot'
  register: scptobmc
  delegate_to: localhost
  ignore_errors: yes

- name: Set wait time on BlueField2
  set_fact:
    wait_time: 15
  when: bf_type == "BF2"

- name: Set wait time on BlueField3
  set_fact:
    wait_time: 10
  when: bf_type != "BF2"

- name: Stop rshim on the BMC
  ansible.builtin.raw: /bin/bash -c "systemctl stop rshim"

- name: Pause for DPU OS Install
  pause:
    minutes: "{{ wait_time }}"

- name: Wait additional time if slow boot is enabled
  pause:
    minutes: "{{ wait_time }}"
  when: (slow_boot is not defined) or (slow_boot|bool == true)
