# Reboot the server

- These tasks are intended to be run on the x86-host.
- They reboot the host `ipmitool power cycle` and waits for it to become accessible again.
- The DPU is expected to be rebooted in this process.
