# Reset NIC config to default

- These tasks are intended to be run on the x86-host.
- They reset the DPU to the default configuration.
