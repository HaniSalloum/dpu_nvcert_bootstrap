# This role will verify that the x86 host is running a supported OS

This role will verify that the host operating system is one of the following:

1. Ubuntu 20.04
2. Ubuntu 22.04
