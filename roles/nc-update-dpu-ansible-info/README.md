# Updates the IP address used by ansible for configuring the DPU
- If the DPU OOB address it can be specified as 0.0.0.0 in the ansibles hosts file.
- The DPU OOB address is then fetched via the host-rshim or DPU-BMC rshim networks.
