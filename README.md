# Playbooks to bootstrap your DPU NvCert Setup

## Quick Start

- Clone this repo to a host with Ansible 2.12.x or later / or follow the Automation Container instructions below
`git clone --recurse-submodules https://gitlab.com/nvidia/networking/bluefield/nvcert/dpu_nvcert_bootstrap.git`
`cd dpu_nvcert_bootstrap`
- Edit the `hosts` file to set the x86 address, username and password
- Run playbooks e.g.`ansible-playbook dpu-nvcert-install-doca.yml`.


## Installation Server
The installation playbooks should be run on a separate server/container. The x86-server, hosting the DPU, is power-cycled multiple times by the installation playbook and is not suitable as the installation server.

## Automation Container
To use ansible you can install the ansible packages on the test server by following these [instructions](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

Alternately you can use a pre-built `automation` container that have the relevant ansible packages already installed. The next few steps will outline how to install the `ansible automation` container -

1. Docker (Linux, Mac, Windows) - Follow this [link](https://docs.docker.com/engine/install/) to the docker install instructions for your platform

2. Pull the automation container from Docker Hub with the following command:
   `sudo docker pull ipspace/automation:ubuntu`

3. Run the container with following command:
   `sudo docker run -it -d ipspace/automation:ubuntu`

4. Next, log into the container with the following command:
   `sudo docker exec -it $(sudo docker ps | grep -i automation | awk -F" " '{print $1}') bash`

   You will see the prompt change to something similar to the following:
   `root@032f1ada86f4:/ansible#`


## Running the playbooks

If you are using an automation container, run these within the container.

1. Clone the dpu nvcert bootstrap repo with the following command:
   `git clone --recurse-submodules https://gitlab.com/nvidia/networking/bluefield/nvcert/dpu_nvcert_bootstrap.git`

2. Change directory:
   `cd dpu_nvcert_bootstrap`

3. Use vim or nano to edit the "hosts" file in this directory

   Change the following settings for the x86 ssh access:

   ```
   ansible_user=<your x86 username>
   ansible_password=<your x86 user password>
   ansible_sudo_pass=<your x86 sudo password>
   x86 ansible_host=<your x86 IP address>
   ```

   Change the following settings for the DPU:
   ```
   dpu ansible_host=<your DPU IP address>
   ```

   Under the `[dpu:vars]` heading, uncomment and change the following if a different (different than the x86) DPU password is needed. On a factory-default card this DPU password is configured as a part of the DPU OS install and used for subsequent DPU remote access.

   ```
   ansible_password=<dpu-password>
   ansible_sudo_pass=<dpu-sudo-password>
   ```

   If `Zero Trust` mode is required change the DPU BMC address and root password. On a factory-default card, the DPU BMC password, specified in the inventory file, is programmed by the installation playbook. The DPU BMC password specified here should meet the following criteria -
   - Minimum length: 13
   - Minimum upper case characters: 1
   - Minimum lower case characters: 1
   - Minimum digits: 1

   ```
   dpu_bmcs ansible_host=<your DPU BMC IP address>
   ```
   ```
   [dpu_bmcs:vars]
   ansible_user=root
   ansible_password=HelloNvidia3D!
   ```

4. Test out the Ansible inventory file with the following command:

   ```
   ansible x86 -m ping --become
   ```

   This should produce an output similar to the following:

   ```
   x86 | SUCCESS => {
       "ansible_facts": {
           "discovered_interpreter_python": "/usr/bin/python3.8"
       },
       "changed": false,
       "ping": "pong"
   }

5. The hosts file in this repo is used to bootstrap one {host, DPU} setup at a time. If you would like to bootstrap multiple hosts/DPUs you can create multiple inventory files by copying the hosts file. And then specify the inventory file name via the command line option `-i <inventory>` when running the play book. For example -
   - `cp hosts inventory-1` and edit inventory-1 to populate the remote access info for host-1/dpu-1.
   - `cp hosts inventory-2` and edit inventory-2 to populate the remote access info for host-2/dpu-2.
   - run the playbook with these inventory files `ansible-playbook <playbook> -i inventory-1` and `ansible-playbook <playbook> -i inventory-2`.

6. Run the appropriate playbook as outlined in the rest of this README file. If a play fails you can use specify `-v` in the command line to get additional debug information for e.g. `ansible-playbook <playbook> -v`.

## DPU modes
These playbooks allow you install the DPU OS in the `Zero Trust mode` or `Host Trusted mode`.

### Host Trusted Mode
- DPU is in the default ECPF/privileged mode.
- Host can ssh into the DPU using the host-rshim network, change NIC config and program the DPU's acceleration engines.
- To install the DPU OS in this mode run `ansible-playbook dpu-nvcert-install-doca.yml`.

### Zero Trust Mode
- DPU access is restricted and only possible via the DPU BMC and DPU OOB/1G port.
- In this mode the host cannot access the DPU via the host-rshim network. The host cannot change the NIC config or program the network accelerators.
- To install the DPU OS in this mode run `ansible-playbook dpu-nvcert-install-doca-zt.yml`.

## Board Type
If you are an external user the DPU board type is always `production` or `PK`. The installation playbooks default to production board-type and install the signed DPU BFB image. The `prod` or `signed` BFB image is the only one published externally.

If you are an internal NVIDIA user you may have received a non-production card and it is important to identify the board type correctly and specify it as a command line argument to the installation playbook. This argument determines the BFB image type installed by the playbook and can be specifed as -
- `-e board_type=DK` for Development cards
- `-e board_type=PK` for Production cards (`default`)
- `-e board_type=QP` for unsigned cards

To determine the board type on a factory default card you can run the follwing commaned on the X86 server -
```
root@x86-host:~# mlxfwmanager
Device #2:
----------

  Device Type:      BlueField3
  Part Number:      900-9D3B6-00CV-AAA_DK_Ax
  Description:      Nvidia BlueField-3 P-Series DPU 200GbE/NDR200 VPI dual-port QSFP112; PCIe Gen5.0 x16 FHHL with x16 PCIe extension option; Crypto Enabled; SB Enabled 32GB on-board DDR; integrated BMC; DK IPN
  PSID:             MT_0000000997
  PCI Device Name:  0000:17:00.0
  Base MAC:         946dae38b246
  Versions:         Current        Available
     FW             32.37.1300     N/A
     PXE            3.7.0102       N/A
     UEFI           14.30.0013     N/A
     UEFI Virtio blk   22.4.0010      N/A
     UEFI Virtio net   21.4.0010      N/A

  Status:           No matching image found

```

- `DK` in the part number or description indicates that it is a development card
- `PK` in the part number or description indicates that it is a production card
- If neither tag is present you need to identify the board type based on Part-Number. Please erach out to the DPU team for that.

Once the DPU is installed and placed in zero-trust or restricted mode `mlxfwmanger` cannot be run from the x86-host. In zero-trust the command can only be run on the DPU Arm.

## Playbooks' Description
### `dpu-nvcert-install-doca.yml`

<img src=./docs/host-trusted.jpg alt="Host Trusted" width=60% height=60% title="Host Title">

This is the playbook to get an x86 host and DPU fully ready to run DOCA applications. This playbook can only be used in the DPU default/ECPF/privileged mode where the host is trusted.

- Installs DOCA software on the x86 hosts
- Installs the DPU BFB image via the host-rshim network. The latest release is installed by default. You can optionally specify the BFB image and location via `doca_bfb` and `bfb_download_url`.
- Updates the DPU firmware as a part of the DPU BFB install
- Sets up the Rshim network to allow ssh from the host to the DPU. After the install you can access the DPU Arm sub-system from the host by running `ssh rshim0`. If you have multiple cards use the rshim-id associated with the card e.g. `ssh rshim1` etc.
- Changes the link type from IB to Ethernet on VPI cards
- Enables `Service Function Chaining` required by `Host Based Networking` if `hbn_enable` is set to true (HBN is disabled by default)

The x86 host is rebooted multiple times during the install.

Arguments/ansible-variables are passed with the `-e` flag.  If you have more than one DPU on the host you can specify the `rshim_num` (default rshim_num is 0) for e.g. to install on the second DPU hosted by the x86 you can specify `-e rshim_num=1`.

You can optionally specify the board type as a command line argument, `-e board_type=DK` or `-e board_type=PK` or `-e board_type=QP`. The default board_type is production/PK.

Install with HBN disabled -
```
ansible-playbook dpu-nvcert-install-doca.yml
```
Install with HBN enabled -
```
ansible-playbook dpu-nvcert-install-doca.yml -e hbn_enable=true
```

The playbook installs the latest release BFB version by default. You can also see `group_vars/all/main.yml` for the image URLs and versions.

### `dpu-nvcert-config-nic-mode.yml`

This is the  playbook to get your DPU to be in NIC mode. To use this playbook you need to specify the DPU BMC and DPU OOB IPv4 addresses in the inventory file (hosts). The DPU BMC needs be in the factory default state or it needs to be accessible via the password provided in the inventory file.

This playbook -
- Change the DPU BMC default password `0penBmc`
- Configure the DPU to be in NIC mode
The host is rebooted for the DPU changes to take effect.
```
ansible-playbook dpu-nvcert-config-nic-mode.yml
```

### `dpu-nvcert-config-dpu-bmc-password.yml`
This playbook can be used to configure the DPU-BMC password on a factory-default card -
- The play assumes that the DPU BMC is accessible via the default, one-time password `0penBmc`
- This default password is changed to the DPU BMC password specified in the inventory file.

The DPU BMC is only used in the Zero-Trust mode. Running this playbook is optional in Zero-Trust mode. Alternately you can run the `dpu-nvcert-install-doca-zt.yml` play directly to change the password and install the DPU OS.

### `dpu-nvcert-install-doca-zt.yml`

<img src=./docs/zero-trust.jpg alt="Zero Trust" width=70% height=70% title="Zero Title">

This is the playbook to get an x86 host and DPU fully ready to run DOCA applications in the `Zero Trust` mode. To use this playbook you need to specify the DPU BMC and DPU OOB IPv4 addresses in the inventory file (hosts). The DPU BMC needs be in the factory default state or it needs to be accessible via the password provided in the inventory file.

The playbook -
- Programs the DPU BMC password on a `factory default` card. The default/one-time, DPU BMC password on a factory-default card is `0penBmc`. If the card is in a factory-default state the password is changed to the DPU-BMC password provided in the inventory file.
- Installs the DPU BFB image via the DPU BMC. The latest release is installed by default. You can optionally specify the BFB image and location via `doca_bfb` and `bfb_download_url`.
- Updates the DPU firmware as a part of the DPU BFB install
- Changes the link type from IB to Ethernet on VPI cards
- Enables `Service Function Chaining` required by `Host Based Networking` if `hbn_enable` is set to true (HBN is disabled by default)
- Puts the DPU in restricted mode, if it is not already in that mode

The x86 host is rebooted multiple times during the install.

Currently there is no programmatic way to identify the BF3 board type via the DPU-BMC so you need to specify board_type as a command line argument, `-e board_type="DK"` or `-e board_type="PK"` or `-e board_type="QP"`. The default board_type is production of "PK".

Install with HBN disabled -
```
ansible-playbook dpu-nvcert-install-doca-zt.yml -e board_type="DK"
```
Install with HBN enabled -
```
ansible-playbook dpu-nvcert-install-doca-zt.yml -e hbn_enable=true -e board_type="DK"
```

The playbook installs the latest release BFB version by default. You can also see `group_vars/all/main.yml` for the image URLs and versions.

### `dpu-nvcert-check-sn.yml`
This playbook can be used to pull and cross-check `Serial Number` from the DPU-BMC, DPU-Arm and server.

Running the playbook is optional. It is typically used to validate the inventory file.

```
ansible-playbook dpu-nvcert-check-sn.yml
```

### `dpu-nvcert-install-hbn-container.yml`

Deploys the DOCA HBN (Host Based Networking) service container on the DPU. DOCA HBN provides classic top of rack (ToR) routing/switching/network overlay capabilities on the DPU for the host.

In the `Host Trusted` mode, if you have more than one DPU on the host you can specify the `rshim_num` (default rshim_num is 0) for e.g. to install the HBN container on the second DPU hosted by the x86 you can specify `-e rshim_num=1`.  In `Zero Trust` mode the rshim_num is not relevant.

You can install either the latest release or a development container -
1. By default the latest release container is installed
2. If you need to install the latest development container instead, specify your NGC API key via `-e ngc_api_key=<your-key>`

You can also see `group_vars/all/main.yml` for the container NGC URLs and versions.
```
ansible-playbook dpu-nvcert-install-hbn-container.yml
```

### `dpu-nvcert-hbn-query.yml`

Pull info about the DOCA HBN (Host Based Networking) service container on the DPU.

In the `Host Trusted` mode, if you have more than one DPU on the host you can specify the `rshim_num` (default rshim_num is 0) for e.g. to query the second DPU hosted by the x86 you can specify `-e rshim_num=1`. In `Zero Trust` mode the rshim_num is not relevant.

```
ansible-playbook dpu-nvcert-hbn-query.yml
```

### `dpu-nvcert-cleanup-zt.yml`

If you onboarded the DPU for certification in Zero Trust mode you need to reset it to factory defaults using this playbook, after certification testing, before returning the DPU to the common pool. This is a `mandatory step` to ensure that the DPU can be installed and provisioned by subsequent users as described in the BlueField User Guide.

This playbook -
- Returns the DPU to the default embedded mode where the host is trusted
- Resets the DPU BMC password to the factory default - `0penBmc` and cold resets the BMC
The host is rebooted for the DPU changes to take effect.
```
ansible-playbook dpu-nvcert-cleanup-zt.yml
```

### `dpu-nvcert-cleanup-nic-mode.yml`

If you onboarded the DPU for certification in NIC mode you need to reset it to factory defaults using this playbook, after certification testing, before returning the DPU to the common pool. This is a `mandatory step` to ensure that the DPU can be installed and provisioned by subsequent users as described in the BlueField User Guide.

This playbook -
- Returns the DPU to the default DPU mode
- Resets the DPU BMC password to the factory default - `0penBmc` and cold resets the BMC
The host is rebooted for the DPU changes to take effect.
```
ansible-playbook dpu-nvcert-cleanup-nic-mode.yml
```

## Authors
- Hala Awisat <hawisat@nvidia.com>
- Anuradha Karuppiah <anuradhak@nvidia.com>
- Mike Courtney <mcourtney@nvidia.com>
- Justin Betz <jubetz@nvidia.com>
