###############################################################################
#
# Copyright 2023 NVIDIA Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
###############################################################################


###############################################################################
# This is the playbook to onboard DPUs in zero-trust mode
# - Installs DOCA software on the x86 host
# - Installs the DPU BFB image via the DPU BMC; the BFB image and location
#   can be optionally specified via `doca_bfb` and `bfb_download_url`
# - Updates the DPU firmware
# - Places the DPU in restrict mode i.e. host cannot access the DPU
# - Enables `Service Function Chaining` required by `Host Based Networking`
#   if `hbn_enable` is set to true (HBN is disabled by default)
###############################################################################


- hosts: localhost
  connection: local
  roles:
    - role: nc-install-local-utils
      tags: [install_local_utils]


# Check if the host version is supported and install host packages
# why do we need to install host DOCA packages in ZT - fixme
- hosts: x86_hosts
  become: yes
  roles:
    # Check if the host has a compatible OS
    - role: nc-host-os-check
      tags: [host_version_check]

    # Install utility packages needed by the nvcert test suite
    - role: nc-install-host-utils
      tags: [install_host_utils]

    # Remove any existing version of DOCA from the host
    - role: nc-uninstall-server-doca
      tags: [install_server_doca]

    # Install DOCA on the host
    - role: nc-install-server-doca
      tags: [install_server_doca]

    # Start MST
    - role: nc-start-mst
      tags: [install_bfb]

    # Stop rshim as we don't want the BMC and host to fight for rshim control
    - role: nc-stop-rshim
      tags: [install_bfb]


# Locate BFB and install it via the DPU BMC
- hosts: dpu_bmcs
  become: yes
  gather_facts: no
  roles:
    # Install DOCA BFB on the DPU WITH_NIC_FW_UPDATE=yes
    - role: nc-zt-bfb-install
      tags: [install_bfb]


# Check if the DPU is up
- hosts: dpus
  become: yes
  gather_facts: no
  roles:
    # Check if the Arm subsytem on the DPU is up
    - role: nc-check-device-up
      tags: [config_dpu]


# Check if the DPU-BMC and DPU Arm serial numbers match
- hosts: dpu_bmcs
  become: yes
  gather_facts: no
  tasks:
    - name: Start rshim on the BMC again
      ansible.builtin.raw: /bin/bash -c "systemctl start rshim"

    - name:
      include_role:
        name: nc-get-dpu-sn

    - name: Print the DPU SN from the DPU Arm
      debug:
        msg: "{{ dpu_sn_from_dpu }}"

    - name: Check if the DPU BMC matches the DPU
      debug:
        msg: "Abort play; SN mismatch DPU BMC {{ dpu_sn_from_bmc }} and DPU {{ dpu_sn_from_dpu }}"
      when: dpu_sn_from_dpu != dpu_sn_from_bmc
      failed_when: dpu_sn_from_dpu != dpu_sn_from_bmc

    - name: Serial numbers from the Host, DPU-BMC, DPU-Arm
      debug:
        msg: "x86: {{ dpu_sn_from_host }}, dpu-bmc: {{ dpu_sn_from_bmc }}, dpu-arm: {{ dpu_sn_from_dpu }}"


# Configure the DPU
- hosts: dpus
  become: yes
  gather_facts: no
  roles:
    # Start MST
    - role: nc-start-mst
      tags: [config_dpu]

    # Get the mst device id
    - role: nc-get-dpu-mst-dev-id
      tags: [config_dpu]

    # Reset NIC config
    - role: nc-reset-dpu-default-config
      tags: [config_dpu]
      # don't reset NIC config if HBN is enabled as that will reset the multi port LAG
      when:
        - (hbn_enable is not defined) or (hbn_enable|bool == false)

    # Put the DPU in DPU mode (it maybe in NIC mode)
    - role: nc-update-dpu-mode
      tags: [config_dpu]

    # Set link type to ethernet
    - role: nc-update-link-type
      tags: [config_dpu]

  post_tasks:
    # get bmc IP and password for the next step
    - name: Save ansible_host variable to bmc_ip
      set_fact:
        bmc_ip: "{{ hostvars['dpu_bmc']['ansible_host'] }}"

    - name: Save ansible_password variable to bmc_password
      set_fact:
        bmc_password: "{{ hostvars['dpu_bmc']['ansible_password'] }}"

    # Put the DPU in restricted mode
    - name: Set the DPU in restricted mode
      shell: "curl -k -u root:{{ bmc_password }} -d '{\"Attributes\":{\"Host Privilege Level\":\"Restricted\"}}' -X PATCH https://{{ bmc_ip }}/redfish/v1/Systems/Bluefield/Bios/Settings"
      tags: [config_dpu]
      delegate_to: localhost


# Power cycle the host after DPU BFB install and firmware upgrade
- hosts: x86_hosts
  become: yes
  roles:
    # Power cycle the host after BFB install and link type change
    - role: nc-reboot-host
      tags: [config_dpu]

# Wait for the Arm subsystem on the DPU to come up after the server power-cycle
- hosts: dpus
  become: yes
  roles:
    - role: nc-check-device-up
      tags: [config_dpu]
