###############################################################################
#
# Copyright 2023 NVIDIA Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
###############################################################################


# This playbook restores the DPU to a factory default state

# 1. Put the DPU back in the default dpu mode
# 2. Reset the DPU BMC password to the factory default

# This playbook cleans up the DPU NIC mode


- hosts: dpu_bmcs
  gather_facts: no
  become: yes
  tasks:
    - name: Save ansible_host variable to bmc_ip
      set_fact:
        bmc_ip: "{{ ansible_host }}"

    - name: Save ansible_password variable to bmc_password
      set_fact:
        bmc_password: "{{ ansible_password }}"

    - name: Set DPU back to be in DPU mode
      shell: "curl -k -u root:{{ bmc_password }} -H 'Content-Type:application/json' -X POST -d '{\"Mode\":\"DpuMode\"}' https://{{ bmc_ip }}/redfish/v1/Systems/Bluefield/Oem/Nvidia/Actions/Mode.Set"
      delegate_to: localhost

    - name: Role- Power cycle the host
      import_role:
        name: nc-reboot-host
      delegate_to: x86

    - name: Factory reset the DPU BMC configuration
      shell: 'ipmitool raw 0x32 0x66' # noqa 305
      delegate_to: dpu

    - name: Cold reset the DPU BMC
      shell: 'ipmitool mc reset cold' # noqa 305
      delegate_to: dpu

    - name: Wait 5 minutes for DPU BMC reset
      pause:
        minutes: 5
